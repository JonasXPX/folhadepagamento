/**
 * Administrador
 */
public class Administrador extends Funcao {

    public static final Double MODIFICADOR_DE_SALARIO_BASE = 1.15;

    private Long cra = 0L;


    public void setCra(Long cra) {
        this.cra = cra;
    }

    public Long setCra() {
        return this.cra;
    }

    @Override
    public Double getSalario() {
        return super.getSalarioBase() * MODIFICADOR_DE_SALARIO_BASE;
    }

    @Override
    public void setNomeFuncao(String nome) {
        super.nomeFuncao = nome;
    }

    @Override
    public Double getPorgentagemDeAcrecimo() {
        return (MODIFICADOR_DE_SALARIO_BASE - 1) * 100;
    }
}