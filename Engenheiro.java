/**
 * Engenheiro
 */
public class Engenheiro extends Funcao { 

    public static final Double MODIFICADOR_DE_SALARIO_BASE = 1.30;
  
    public Long crea = 0L;


    public void setCrea(Long crea) {
        this.crea = crea;
    }

    public Long getCrea(){
        return this.crea;
    }

    @Override
    public Double getSalario() {
        return super.getSalarioBase() * MODIFICADOR_DE_SALARIO_BASE;
    }

    @Override
    public void setNomeFuncao(String nome) {
        super.nomeFuncao = nome;
    }

    @Override
    public Double getPorgentagemDeAcrecimo() {
        return (MODIFICADOR_DE_SALARIO_BASE - 1) * 100;
    }
} 