/**
 * Analista
 */
public class Analista extends Funcao {

    public static final Double MODIFICADOR_DE_SALARIO_BASE = 1.20;
    
    @Override
    public Double getSalario() {
        return super.getSalarioBase() * MODIFICADOR_DE_SALARIO_BASE;
    }
    
    @Override
    public void setNomeFuncao(String nome) {
        super.nomeFuncao = nome;
    }
    
    @Override
    public Double getPorgentagemDeAcrecimo() {
        return (MODIFICADOR_DE_SALARIO_BASE - 1) * 100;
    }
}