/**
 * Funcao
 */
public abstract class Funcao {

    public static final Double SALARIO_BASE = 3500.00;
    public String nomeFuncao;

    public Double getSalarioBase() {
        return SALARIO_BASE;
    }

    public Double getSalario(){
        return getSalarioBase();
    }

    public abstract void setNomeFuncao(String nome);

    public String getNomeFuncao() {
        return nomeFuncao;
    }
    public Double getPorgentagemDeAcrecimo(){return null;}
}