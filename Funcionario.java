/**
 * Funcionario
 */
public class Funcionario{

    private Funcao funcao;
    private String nome;
    
    public Funcionario(){}
    public Funcionario(Funcao funcao, String nome){
        this.funcao = funcao;
        this.nome = nome;
    }

    public void setFuncao(Funcao funcao) {
        this.funcao = funcao;
    }

    public Funcao getFuncao() {
        return this.funcao;
    }

    public Double getSalario() {
        return this.funcao.getSalario();
    }

    public String getNome(){
        return this.nome;
    }


}