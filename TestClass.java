
/**
 * TestClass
 */
public class TestClass {

    public static void main(String[] args) {
        Administrador adm = new Administrador();
        adm.setNomeFuncao("Adiministrador");

        Engenheiro eng = new Engenheiro();
        eng.setNomeFuncao("Engenheiro");

        Analista anl = new Analista();
        anl.setNomeFuncao("Analista");
        
        Funcionario[] funcionarios = {
            new Funcionario(adm, "Jonas"),
            new Funcionario(eng, "Pedro"),
            new Funcionario(anl, "Joao"),
            new Funcionario(eng, "Maria"),
            new Funcionario(anl, "Keila")
        };

        for (Funcionario funcionario : funcionarios) {
            StringBuilder sb = new StringBuilder();
            sb.append("+-------------Folha de Pagamento--------------+\n")
            .append("| Funcionário: ").append(funcionario.getNome()).append(" | Cargo: ").append(funcionario.getFuncao().getNomeFuncao()).append("\n")
            .append("| Salário Base: ").append(funcionario.getFuncao().getSalarioBase()).append("\n")
            .append("| Salario liquido: R$ ").append(funcionario.getSalario()).append("\n");

            System.out.println(sb.toString());
        }

    }
    
}